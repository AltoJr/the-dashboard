import { Component, Inject, Input, NgZone, OnInit, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

// amCharts imports
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import { PiechartModel } from 'src/app/models/piechart.model';

@Component({
  selector: 'app-piechart',
  templateUrl: './piechart.component.html',
  styleUrls: ['./piechart.component.scss']
})
export class PiechartComponent {

  private chart: am4charts.XYChart;
  @Input('piechartData') piechartData: Array<PiechartModel>;

  constructor(@Inject(PLATFORM_ID) private platformId, private zone: NgZone) {}

  // Run the function only in the browser
  browserOnly(f: () => void) {
    if (isPlatformBrowser(this.platformId)) {
      this.zone.runOutsideAngular(() => {
        f();
      });
    }
  }

  ngAfterViewInit() {
    this.initPiechart();
  }

  ngOnChanges() {
    this.initPiechart();
  }

  ngOnDestroy() {
    this.browserOnly(() => {
      if (this.chart) {
        this.chart.dispose();
      }
    });
  }

  initPiechart() {
    this.browserOnly(() => {
      let chart = am4core.create("piechartdiv", am4charts.PieChart);
      chart.data = this.piechartData;

      // configure
      let pieSeries = chart.series.push(new am4charts.PieSeries());
      pieSeries.dataFields.value = "value";
      pieSeries.dataFields.category = "name";
      chart.innerRadius = am4core.percent(40);
    });
  }

}
