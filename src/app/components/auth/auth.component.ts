import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserModel } from 'src/app/models/user.model';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  private userModel: UserModel;
  loginForm: FormGroup;
  errorLogin: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router
    ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  onSubmit() {

    this.userModel = this.loginForm.value;
    this.errorLogin = false;

    this.authService.login(this.userModel)
      .subscribe(success => {
        if (success) {
          this.router.navigate(['/dashboard']);
        } else {
          this.errorLogin = true;
          console.log('user not found');
        }
      });

    console.warn(this.loginForm.value);
  }

}
