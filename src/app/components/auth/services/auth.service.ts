import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tokenName } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { of, Observable } from 'rxjs';
import { catchError, mapTo, tap } from 'rxjs/operators';
import { UserModel } from 'src/app/models/user.model';
import { environment } from 'src/environments/environment';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  login(userModel: UserModel): Observable<boolean> {
    return this.http.post<any>(`${environment.baseUrl}api/account/login`, userModel, httpOptions)
    .pipe(
      tap(token => this.processLogin(token)),
      mapTo(true),
      catchError(error => {
        // alert(error.error);
        return of(false);
      }));
  }

  logout() {
    this.processLogout();
  }

  isLoggedIn() {
    return !!this.getToken();
  }

  getToken() {
    return localStorage.getItem('TOKEN');
  }

  private processLogin(token: string) {
    localStorage.setItem('TOKEN', token);
  }

  private processLogout() {
    localStorage.removeItem('TOKEN');
  }
}
