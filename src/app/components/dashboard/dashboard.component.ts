import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BarchartModel } from 'src/app/models/barchart.model';
import { PiechartModel } from 'src/app/models/piechart.model';
import { DashboardService } from 'src/app/services/dashboard.service';
import { AuthService } from '../auth/services/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  data: any;
  piechartList: Array<PiechartModel>;
  barchartList: Array<BarchartModel>;

  constructor(
    private authService: AuthService,
    private router: Router,
    private dashboardService: DashboardService
  ) { }

  ngOnInit() {
    this.getStatisticData();
  }

  onLogout() {
    this.authService.logout();
    this.router.navigate(['/login']);
  }

  getStatisticData() {
    this.dashboardService.getStatisticData()
      .subscribe(
        (data) => {
          console.log('Data Retreived', data);
          this.data = data;
          this.piechartList = this.data.chartDonut;
          this.barchartList = this.data.chartBar;
        },
        (error) => {
            console.error(error);
        }
      );
  }

}
